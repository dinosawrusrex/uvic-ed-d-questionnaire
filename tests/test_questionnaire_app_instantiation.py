import unittest
import os, sys
from flask import url_for

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from questionnaire import create_app, db
from questionnaire.database import (
    populate_tutorial_topic,
    populate_form_headers,
    populate_instruments,
    populate_question_bank,
)
from questionnaire.models import (
    Question,
    Instrument,
    FormHeader,
    TutorialTopic,
    Student,
    Session,
    Response,
)


def create_test_app():
    app = create_app()
    app.config["TESTING"] = True
    app.config["DEBUG"] = True
    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = "postgresql:///test-uvic-ed-d-questionnaire-db"
    app.config["WTF_CSRF_ENABLED"] = False
    return app


def init_database():
    populate_tutorial_topic()
    populate_form_headers()
    populate_instruments()
    populate_question_bank()


class BaseTestCase(unittest.TestCase):
    """ Base test case that will be inherited by subsequent test cases.
    Contains set up method to create an app instance and set up database and
    tear down method to destroy database and remove application context.
    """

    @classmethod
    def setUpClass(cls):
        """ Sets up application and populates database with required data.
        """
        cls.app = create_test_app()
        db.init_app(cls.app)
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        db.create_all()
        with cls.app.app_context():
            init_database()
            db.session.commit()
        cls.db = db
        cls.app = cls.app.test_client()

    @classmethod
    def tearDownClass(cls):
        """ Clears database
        """
        db.session.close_all()
        db.drop_all()
        cls.app_context.pop()


class ApplicationTest(BaseTestCase):
    """ Checks for the application and database instances created for testing
    purposes.

    Application should return the index page if instance is created
    successfully.

    The Question, Instrument, FormHeader, and TutorialTopic tables
    are pre-populated after creating the database. They should not be empty if
    instantiation and population are successful.
    """

    # Set up application and context to test database
    def test_main_page(self):
        """ Checks if instance of application is running and accessible. If the
        main page's endpoint is hit, the index page should be provided.
        """
        response = self.app.get("/", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Welcome to the site for UVic ED-D 101 surveys!", response.data)

    # Tests whether database created and populated appropriately
    def test_question_instrument_formheader_tutorial_topic_populated(self):
        """ If database is populated successfully, following four tables should
        not be empty.
        """
        self.assertNotEqual(Question.query.all(), [])
        self.assertNotEqual(Instrument.query.all(), [])
        self.assertNotEqual(FormHeader.query.all(), [])
        self.assertNotEqual(TutorialTopic.query.all(), [])

    def test_student_session_response_empty(self):
        """ Database population should not insert data into student, session,
        and response tables.
        """
        self.assertEqual(Student.query.all(), [])
        self.assertEqual(Session.query.all(), [])
        self.assertEqual(Response.query.all(), [])


if __name__ == "__main__":
    unittest.main()
