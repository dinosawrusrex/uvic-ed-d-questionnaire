import itertools, os, random, statistics, sys
import unittest
from test_questionnaire_app_instantiation import BaseTestCase
from sqlalchemy import or_

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from questionnaire.blueprints.results_display import (
    compute_mental_health_continuum_output,
    calculate_likert_mean,
    calculate_truefalse_mean,
    calculate_checkbox_mean,
    compute_score,
)
from questionnaire.models import Instrument, Response, Session, TutorialTopic


class ComputeMentalHealthContinuumOutputTest(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

        cls.default_responses = {
            question.code: 0
            for instrument in Instrument.query.filter_by(name="Mental Health").all()
            for question in instrument.questions
        }

    def test_for_fluorishing(self):
        """ Tests to check if conditions for fluorishing means result is
        fluorishing.

        Conditions are one or more of three ewb has score of 4 or 5, and 6 or
        more swb and pwb has score of 4 or 5.
        """
        scores = [4, 5]
        ewb_counts = [1, 2]
        swb_and_pwb_counts = [6, 7]

        for responses in self.response_data(
            scores, scores, ewb_counts, swb_and_pwb_counts
        ):
            with self.subTest(combination=responses):
                self.app.get("/questionnaire/s5_socemo")
                output_response = self.app.post(
                    "/output", data=responses, follow_redirects=False
                )
                responses_from_session_page_response = self.app.get(
                    f"/responses/{Session.query.first().id}", follow_redirects=False
                )

                for response in [output_response, responses_from_session_page_response]:
                    self.assertEqual(response.status_code, 200)
                    self.assertIn(b"<b>Fluorishing</b>", response.data)
                    self.assertEqual(response.data.count(b"<b>Fluorishing</b>"), 1)

                    self.assertEqual(
                        compute_mental_health_continuum_output(Session.query.first().id),
                        "Fluorishing",
                    )
                    self.assertEqual(len(Session.query.all()), 1)

                    mhc_responses = {
                        response.question_code: response.value
                        for response in Response.query.all()
                        if response.question_code in self.default_responses
                    }
                    self.assertEqual(len(mhc_responses), len(self.default_responses))

                Response.query.delete()
                Session.query.delete()

        self.assertEqual(set(self.default_responses.values()), {0})

    def test_for_languishing(self):
        """ Tests to check if conditions for languishing means result is
        languishing.

        Conditions are one or more of three ewb has score of 1 or 2, and 6 or
        more swb and pwb has score of 1 or 2.
        """
        scores = [1, 2]
        ewb_counts = [1, 2]
        swb_and_pwb_counts = [6, 7]

        for responses in self.response_data(
            scores, scores, ewb_counts, swb_and_pwb_counts
        ):
            with self.subTest(combination=responses):
                self.app.get(f"/questionnaire/s5_socemo")
                output_response = self.app.post(
                    "/output", data=responses, follow_redirects=False
                )
                responses_from_session_page_response = self.app.get(
                    f"/responses/{Session.query.first().id}", follow_redirects=False
                )

                for response in [output_response, responses_from_session_page_response]:
                    self.assertEqual(response.status_code, 200)
                    self.assertIn(b"<b>Languishing</b>", response.data)
                    self.assertEqual(response.data.count(b"<b>Languishing</b>"), 1)

                    self.assertEqual(
                        compute_mental_health_continuum_output(Session.query.first().id),
                        "Languishing",
                    )
                    self.assertEqual(len(Session.query.all()), 1)

                    mhc_responses = {
                        response.question_code: response.value
                        for response in Response.query.all()
                        if response.question_code in self.default_responses
                    }
                    self.assertEqual(len(mhc_responses), len(self.default_responses))

                Response.query.delete()
                Session.query.delete()

        self.assertEqual(set(self.default_responses.values()), {0})

    def test_for_moderate_one(self):
        """ Tests to check if conditions for moderate means result is
        moderate.

        When languishing or fluorishing conditions are not met, it is moderate.
        """
        scores = [4]
        ewb_counts = [1, 2]
        swb_and_pwb_counts = [4, 5]

        for responses in self.response_data(
            scores, scores, ewb_counts, swb_and_pwb_counts
        ):
            with self.subTest(combination=responses):
                self.app.get(f"/questionnaire/s5_socemo")
                output_response = self.app.post(
                    "/output", data=responses, follow_redirects=False
                )
                responses_from_session_page_response = self.app.get(
                    f"/responses/{Session.query.first().id}", follow_redirects=False
                )

                for response in [output_response, responses_from_session_page_response]:

                    self.assertEqual(response.status_code, 200)
                    self.assertIn(b"<b>Moderate</b>", response.data)
                    self.assertEqual(response.data.count(b"<b>Moderate</b>"), 1)

                    self.assertEqual(
                        compute_mental_health_continuum_output(Session.query.first().id),
                        "Moderate",
                    )
                    self.assertEqual(len(Session.query.all()), 1)

                    mhc_responses = {
                        response.question_code: response.value
                        for response in Response.query.all()
                        if response.question_code in self.default_responses
                    }
                    self.assertEqual(len(mhc_responses), len(self.default_responses))

                Response.query.delete()
                Session.query.delete()

        self.assertEqual(set(self.default_responses.values()), {0})

    def test_for_moderate_two(self):
        """ Tests to check if conditions for moderate means result is
        moderate.

        When languishing or fluorishing conditions are not met, it is moderate.
        """
        ewb_scores = [1, 2]
        swb_and_pwb_scores = [4, 5]
        ewb_counts = [1, 2]
        swb_and_pwb_counts = [5, 6, 7]

        for responses in self.response_data(
            ewb_scores, swb_and_pwb_scores, ewb_counts, swb_and_pwb_counts
        ):
            with self.subTest(combination=responses):
                self.app.get(f"/questionnaire/s5_socemo")
                output_response = self.app.post(
                    "/output", data=responses, follow_redirects=False
                )
                responses_from_session_page_response = self.app.get(
                    f"/responses/{Session.query.first().id}", follow_redirects=False
                )

                for response in [output_response, responses_from_session_page_response]:
                    self.assertEqual(response.status_code, 200)
                    self.assertIn(b"<b>Moderate</b>", response.data)
                    self.assertEqual(response.data.count(b"<b>Moderate</b>"), 1)

                    self.assertEqual(
                        compute_mental_health_continuum_output(Session.query.first().id),
                        "Moderate",
                    )
                    self.assertEqual(len(Session.query.all()), 1)

                    mhc_responses = {
                        response.question_code: response.value
                        for response in Response.query.all()
                        if response.question_code in self.default_responses
                    }
                    self.assertEqual(len(mhc_responses), len(self.default_responses))

                Response.query.delete()
                Session.query.delete()

        self.assertEqual(set(self.default_responses.values()), {0})

    def test_for_moderate_three(self):
        """ Tests to check if conditions for moderate means result is
        moderate.

        When languishing or fluorishing conditions are not met, it is moderate.
        """
        ewb_scores = [4, 5]
        swb_and_pwb_scores = [1, 2]
        ewb_counts = [1, 2]
        swb_and_pwb_counts = [5, 6, 7]

        for responses in self.response_data(
            ewb_scores, swb_and_pwb_scores, ewb_counts, swb_and_pwb_counts
        ):
            with self.subTest(combination=responses):
                self.app.get(f"/questionnaire/s5_socemo")
                output_response = self.app.post(
                    "/output", data=responses, follow_redirects=False
                )
                responses_from_session_page_response = self.app.get(
                    f"/responses/{Session.query.first().id}", follow_redirects=False
                )

                for response in [output_response, responses_from_session_page_response]:
                    self.assertEqual(response.status_code, 200)
                    self.assertIn(b"<b>Moderate</b>", response.data)
                    self.assertEqual(response.data.count(b"<b>Moderate</b>"), 1)

                    self.assertEqual(
                        compute_mental_health_continuum_output(Session.query.first().id),
                        "Moderate",
                    )
                    self.assertEqual(len(Session.query.all()), 1)

                    mhc_responses = {
                        response.question_code: response.value
                        for response in Response.query.all()
                        if response.question_code in self.default_responses
                    }
                    self.assertEqual(len(mhc_responses), len(self.default_responses))

                Response.query.delete()
                Session.query.delete()

        self.assertEqual(set(self.default_responses.values()), {0})

    def test_for_moderate_four(self):
        """ Tests to check if conditions for moderate means result is
        moderate.

        When languishing or fluorishing conditions are not met, it is moderate.
        """
        ewb_scores = []
        swb_and_pwb_scores = []
        ewb_counts = []
        swb_and_pwb_counts = []

        for responses in self.response_data(
            ewb_scores, swb_and_pwb_scores, ewb_counts, swb_and_pwb_counts
        ):
            with self.subTest(combination=responses):
                self.app.get(f"/questionnaire/s5_socemo")
                output_response = self.app.post(
                    "/output", data=responses, follow_redirects=False
                )
                responses_from_session_page_response = self.app.get(
                    f"/responses/{Session.query.first().id}", follow_redirects=False
                )

                for response in [output_response, responses_from_session_page_response]:
                    self.assertEqual(response.status_code, 200)
                    self.assertIn(b"<b>Moderate</b>", response.data)
                    self.assertEqual(response.data.count(b"<b>Moderate</b>"), 1)

                    self.assertEqual(
                        compute_mental_health_continuum_output(Session.query.first().id),
                        "Moderate",
                    )
                    self.assertEqual(len(Session.query.all()), 1)

                    mhc_responses = {
                        response.question_code: response.value
                        for response in Response.query.all()
                        if response.question_code in self.default_responses
                    }
                    self.assertEqual(len(mhc_responses), len(self.default_responses))

                Response.query.delete()
                Session.query.delete()

        self.assertEqual(set(self.default_responses.values()), {0})

    def response_data(
        self, ewb_scores, swb_and_pwb_scores, ewb_counts, swb_and_pwb_counts
    ):
        """ Generates a list of response values from the provided arguments.

        Scores is a list of scores. For example, to test for fluorishing result,
        we need scores of 4 or 5.

        ewb counts is the number of ewb codes we would like to assign the given
        score.

        swb_and_pwb_counts functions the same way as the ewb counts.
        """
        for combination in itertools.product(
            ewb_counts, swb_and_pwb_counts, ewb_scores, swb_and_pwb_scores
        ):
            ewb_codes = self.sample_mhe_question_codes(combination[0])
            swb_and_pwb_codes = self.sample_swb_and_pwb_question_codes(combination[1])
            ewb_score = combination[2]
            swb_and_pwb_score = combination[3]

            responses = self.default_responses.copy()

            for question_code in responses:
                if question_code in ewb_codes:
                    responses[question_code] = ewb_score
                elif question_code in swb_and_pwb_codes:
                    responses[question_code] = swb_and_pwb_score
                else:
                    responses[question_code] = 3

            yield responses

    @staticmethod
    def sample_mhe_question_codes(number_of_questions):
        """Returns a list of ewb question codes randomly sampled"""
        return random.sample(
            [
                question.code
                for question in Instrument.query.filter_by(code="mh-e").first().questions
            ],
            number_of_questions,
        )

    @staticmethod
    def sample_swb_and_pwb_question_codes(number_of_questions):
        """Returns a list of swb and pwb question codes sampled."""
        return random.sample(
            [
                question.code
                for instrument in Instrument.query.filter(
                    or_(Instrument.code == "mh-s", Instrument.code == "mh-p")
                ).all()
                for question in instrument.questions
            ],
            number_of_questions,
        )


class ComputeLikertMeanTest(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

        cls.instrument_codes_to_test = [
            instrument.code
            for instrument in Instrument.query.filter(
                Instrument.response_type == "likert",
                Instrument.scoring_method == "average",
                Instrument.name != "Mental Health",
            ).all()
        ]

    def test_likert_mean_calculation(self):
        for topic in TutorialTopic.query.all():
            for instrument in topic.instruments:
                self.app.get(f"/questionnaire/{topic.code}")
                if instrument.code in self.instrument_codes_to_test:
                    response_data = self.response_data(instrument.code)
                    with self.subTest(instrument=instrument.code):
                        output_response = self.app.post(
                            "/output", data=response_data, follow_redirects=False
                        )
                        responses_from_session_page_response = self.app.get(
                            f"/responses/{Session.query.first().id}",
                            follow_redirects=False,
                        )

                        for response in [
                            output_response,
                            responses_from_session_page_response,
                        ]:
                            mean_of_response_data_provided = round(
                                statistics.mean(response_data.values()), 2
                            )

                            self.assertEqual(response.status_code, 200)
                            self.assertIn(
                                bytes(str(mean_of_response_data_provided), "utf-8"),
                                response.data,
                            )

                            self.assertEqual(
                                mean_of_response_data_provided,
                                calculate_likert_mean(
                                    instrument, Session.query.first().id
                                ),
                            )

                            self.assertEqual(
                                mean_of_response_data_provided,
                                compute_score(instrument, Session.query.first().id),
                            )

                            self.assertEqual(
                                len(response_data), int(instrument.number_of_questions)
                            )
                            self.assertEqual(len(Session.query.all()), 1)

                            likert_responses = {
                                response.question_code: response.value
                                for response in Response.query.all()
                                if response.question_code in response_data
                            }
                            self.assertEqual(len(likert_responses), len(response_data))

                Response.query.delete()
                Session.query.delete()

    @staticmethod
    def response_data(instrument_code):
        instrument = Instrument.query.filter_by(code=instrument_code).first()
        return {
            question.code: random.choice(range(1, int(instrument.likert_max)))
            for question in instrument.questions
        }


class ComputeTruefalseMeanTest(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

        cls.instruments = Instrument.query.filter_by(name="Metacognitive Awareness").all()

        cls.responses = {
            question.code: random.choice([True, False])
            for instrument in cls.instruments
            for question in instrument.questions
        }

        cls.app.get("/questionnaire/s7_meta")
        cls.output_response = cls.app.post(
            "/output", data=cls.responses, follow_redirects=False
        )
        cls.responses_from_session_page_response = cls.app.get(
            f"/responses/{Session.query.first().id}", follow_redirects=False
        )

    def test_responses_and_session(self):
        self.assertEqual(
            len(self.responses),
            sum(int(instrument.number_of_questions) for instrument in self.instruments),
        )
        self.assertEqual(len(Response.query.all()), len(self.responses))
        self.assertEqual(len(Session.query.all()), 1)

        database_responses = {
            response.question_code: response.value for response in Response.query.all()
        }

        for code in self.responses:
            self.assertEqual(str(self.responses[code]), database_responses[code])

    def test_truefalse(self):
        for instrument in self.instruments:
            summary = round(
                statistics.mean(
                    5 if self.responses[question.code] else 0
                    for question in instrument.questions
                ),
                2,
            )
            self.assertEqual(
                summary, calculate_truefalse_mean(instrument, Session.query.first().id)
            )

            self.assertEqual(summary, compute_score(instrument, Session.query.first().id))

            summary_in_table = bytes(
                "<b>{summary}</b>".format(
                    subfactor=instrument.subfactor, summary=str(summary)
                ),
                "utf-8",
            )
            for response in [
                self.output_response,
                self.responses_from_session_page_response,
            ]:
                self.assertIn(summary_in_table, response.data)


class ComputeCheckboxMeanTest(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

        cls.instruments = Instrument.query.filter(
            Instrument.response_type == "checkbox", Instrument.scoring_method == "average"
        ).all()

        cls.responses = {
            question.code: random.choice([True, None])
            for instrument in cls.instruments
            for question in instrument.questions
        }

        cls.app.get("/questionnaire/s2_rlq")
        cls.output_response = cls.app.post(
            "/output", data=cls.responses, follow_redirects=False
        )
        cls.responses_from_session_page_response = cls.app.get(
            f"/responses/{Session.query.first().id}", follow_redirects=False
        )

    def test_responses_and_session(self):
        self.assertEqual(
            len(self.responses),
            sum(int(instrument.number_of_questions) for instrument in self.instruments),
        )
        self.assertEqual(len(Response.query.all()), len(self.responses) + 12)
        self.assertEqual(len(Session.query.all()), 1)

        database_responses = {
            response.question_code: response.value for response in Response.query.all()
        }

        for code in self.responses:
            self.assertEqual(str(self.responses[code]), str(database_responses[code]))

    def test_checkbox_mean(self):
        for instrument in self.instruments:
            with self.subTest(instrument=instrument.code):
                summary = round(
                    statistics.mean(
                        5 if self.responses[question.code] else 0
                        for question in instrument.questions
                    ),
                    2,
                )
                self.assertEqual(
                    summary, calculate_checkbox_mean(instrument, Session.query.first().id)
                )

                self.assertEqual(
                    summary, compute_score(instrument, Session.query.first().id)
                )

            summary_in_table = bytes(f'<dd class="col-6">{str(summary)}</dd', "utf-8")

            self.assertIn(summary_in_table, self.output_response.data)
            self.assertIn(
                summary_in_table, self.responses_from_session_page_response.data
            )


if __name__ == "__main__":
    unittest.main()
