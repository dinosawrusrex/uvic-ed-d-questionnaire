import os, random, sys
import unittest
from test_questionnaire_app_instantiation import BaseTestCase

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from questionnaire.blueprints.results_display import produce_output_view
from questionnaire.models import (
    Instrument,
    Question,
    Response,
    Session,
    TutorialTopic,
)


class RecordResponsesTests(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

        cls.question_codes_with_responses = random.sample(
            [question.code for question in Question.query.all()],
            int(len(Question.query.all()) / 2),
        )

    def test_responses_recorded(self):
        """ Tests to check if responses posted to questionnaire and output
        routes are recorded into the database and retrievable.

        Checks include:
        - number of created responses compared to number of questions per
          tutorial topic
        - number of created responses compared to number of items in Response
          table
        - number of session should be 1
        - response codes should be 200
        - Tutorial topic result string should be in response data of both output
          page and response by session pages
        - responses created should match responses in database
        """
        for topic in TutorialTopic.query.all():
            with self.subTest(topic=topic.code):
                response_data = {
                    question.code: self.response_according_to_instrument_response_type(
                        instrument
                    )
                    if question.code in self.question_codes_with_responses
                    else None
                    for instrument in topic.instruments
                    for question in instrument.questions
                }

                # Submit responses for the survey
                self.app.get(f"/questionnaire/{topic.code}")
                output_page_response = self.app.post(
                    "/output", data=response_data, follow_redirects=False
                )
                responses_from_session_page_response = self.app.get(
                    f"/responses/{Session.query.first().id}",
                    follow_redirects=False,
                )

                self.assertEqual(
                    len(response_data),
                    sum(
                        instrument.number_of_questions
                        for instrument in topic.instruments
                    ),
                )

                self.assertEqual(len(response_data), len(Response.query.all()))

                self.assertEqual(len(Session.query.all()), 1)

                self.assertEqual(output_page_response.status_code, 200)
                self.assertEqual(
                    responses_from_session_page_response.status_code, 200
                )
                title = topic.topic + " Results"
                self.assertIn(bytes(title, "utf-8"), output_page_response.data)
                self.assertIn(
                    bytes(title, "utf-8"),
                    responses_from_session_page_response.data,
                )

                responses_from_database = {
                    response.question_code: response.value
                    for response in Response.query.all()
                }
                for question_code in response_data:
                    self.assertEqual(
                        response_data[question_code],
                        responses_from_database[question_code],
                    )

                Response.query.delete()
                Session.query.delete()

    @staticmethod
    def response_according_to_instrument_response_type(instrument):
        if instrument.response_type == "likert":
            return str(instrument.likert_max)
        elif instrument.response_type == "checkbox":
            return "True"
        else:
            return random.choice(["True", "False"])


class ReportDescriptionTests(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

    def test_rendered_report_description(self):
        """ Tests of the rendered HTML for output and responses by session
        pages.

        Checks include:
        - response code should be 200
        - report description for all instruments in the tutorial topic should be
          present

        There are escape codes to deal with that are created by Jinja, Python,
        and God know who that needed to be dealt with.
        """
        for topic in TutorialTopic.query.all():

            self.app.get(f"/questionnaire/{topic.code}")
            output_response = self.app.post("/output", follow_redirects=False)
            responses_from_session_page_response = self.app.get(
                f"/responses/{Session.query.first().id}", follow_redirects=False
            )

            self.assertEqual(output_response.status_code, 200)
            self.assertEqual(
                responses_from_session_page_response.status_code, 200
            )

            for instrument in topic.instruments:
                with self.subTest(topic=topic.code, instrument=instrument.code):
                    if (
                        instrument.report_description
                    ):  # mav does not have report
                        self.assertIn(
                            bytes(
                                instrument.report_description.replace(
                                    "'", ""
                                ).replace("experience\n", "experience "),
                                "utf-8",
                            ).replace(b"\xe2\x80\x99", b""),
                            output_response.data.replace(b"&#39;", b"")
                            .replace(b"\xe2\x80\x99", b"")
                            .replace(b"experience<br>", b"experience ")
                            .replace(b"'", b""),
                        )

                        self.assertIn(
                            bytes(
                                instrument.report_description.replace(
                                    "'", ""
                                ).replace("experience\n", "experience "),
                                "utf-8",
                            ).replace(b"\xe2\x80\x99", b""),
                            responses_from_session_page_response.data.replace(
                                b"&#39;", b""
                            )
                            .replace(b"\xe2\x80\x99", b"")
                            .replace(b"experience<br>", b"experience ")
                            .replace(b"'", b""),
                        )

            Response.query.delete()
            Session.query.delete()


class ProduceOutputViewTests(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

    def test_produce_output_view_except_for_summaries(self):
        """ Test produce output view method except for the summary key:value.

        Checks if title is rendered.

        Checks if correct HTML file name selected.
        """
        for topic in TutorialTopic.query.all():

            self.app.get(f"/questionnaire/{topic.code}")
            output_response = self.app.post("/output", follow_redirects=False)
            responses_from_session_page_response = self.app.get(
                f"/responses/{Session.query.first().id}", follow_redirects=False
            )

            self.assertEqual(output_response.status_code, 200)
            self.assertEqual(
                responses_from_session_page_response.status_code, 200
            )

            output_view = produce_output_view(
                topic.code, Session.query.first().id
            )

            self.assertEqual(topic.topic, output_view["title"])

            self.assertIn(
                bytes(output_view["title"], "utf-8"), output_response.data
            )
            self.assertIn(
                bytes(output_view["title"], "utf-8"),
                responses_from_session_page_response.data,
            )

            if any(map((lambda code: code in topic.code), ["s1", "s4", "s5"])):
                self.assertEqual(
                    "summary_views/output.html", output_view["template"]
                )
            elif "s2" in topic.code:
                self.assertEqual(
                    "summary_views/s2_output.html", output_view["template"]
                )
            elif "s6" in topic.code:
                self.assertEqual(
                    "summary_views/s6_output.html", output_view["template"]
                )
            elif "s7" in topic.code:
                self.assertEqual(
                    "summary_views/s7_output.html", output_view["template"]
                )

            Response.query.delete()
            Session.query.delete()


if __name__ == "__main__":
    unittest.main()
