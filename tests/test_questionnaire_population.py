import os, sys
import unittest
from test_questionnaire_app_instantiation import BaseTestCase

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from questionnaire.blueprints.questionnaire import (
    list_of_intermediate_labels,
    formheaders_with,
    questions_from_instruments,
    instrument_view_object,
    section_by_formheader,
    formheaders_and_instruments,
)
from questionnaire.models import FormHeader, Instrument, Student, TutorialTopic


class ListingIntermediateLabelsTest(BaseTestCase):
    """ Checks intermediate labels of instruments.

    A simple test for the number of instruments with and without intermediate
    labels. Then two tests for two instruments to check if the list of
    intermediate labels are the same as the intermediate labels from the CSV.
    """

    def test_number_of_instruments_with_and_without_intermediate_labels(self):
        intermediate_labels_column = [
            list_of_intermediate_labels(instrument)
            for instrument in Instrument.query.all()
        ]
        self.assertEqual(intermediate_labels_column.count(None), 42)
        self.assertEqual(len(intermediate_labels_column) - 42, 7)

    def test_number_of_intermediate_labels_in_study_process_instrument(self):

        intermediate_labels = sorted(
            [
                "sometimes true of me",
                "true of me about half the time",
                "frequently true of me",
            ]
        )

        for instrument in Instrument.query.filter_by(
            name="Study Process"
        ).all():
            with self.subTest(instrument=instrument):
                self.assertEqual(
                    sorted(list_of_intermediate_labels(instrument)),
                    intermediate_labels,
                )

    def test_number_of_intermediate_labels_in_mental_health_instrument(self):

        intermediate_labels = sorted(
            ["once or twice", "three or four times", "almost every day"]
        )

        for instrument in Instrument.query.filter_by(
            name="Mental Health"
        ).all():
            with self.subTest(instrument=instrument):
                self.assertEqual(
                    sorted(list_of_intermediate_labels(instrument)),
                    intermediate_labels,
                )


class FormHeadersWithTutorialTopicCodeTest(BaseTestCase):
    """Checks if right formheaders are obtained with formheaders_with method."""

    def test_s1(self):
        formheader_codes = sorted(
            [
                header.code
                for header in TutorialTopic.query.filter_by(code="s1_learn")
                .first()
                .form_headers
            ]
        )
        self.assertEqual(formheader_codes, ["s1-1", "s1-2", "s1-3", "s1-4"])

    def test_s2(self):
        formheader_codes = sorted(
            [
                header.code
                for header in TutorialTopic.query.filter_by(code="s2_rlq")
                .first()
                .form_headers
            ]
        )
        self.assertEqual(formheader_codes, ["s2-1", "s2-2", "s2-3", "s2-4"])

    def test_s4(self):
        formheader_codes = sorted(
            [
                header.code
                for header in TutorialTopic.query.filter_by(code="s4_beh")
                .first()
                .form_headers
            ]
        )
        self.assertEqual(formheader_codes, ["s4-1", "s4-2", "s4-3", "s4-4"])

    def test_s5(self):
        formheader_codes = sorted(
            [
                header.code
                for header in TutorialTopic.query.filter_by(code="s5_socemo")
                .first()
                .form_headers
            ]
        )
        self.assertEqual(formheader_codes, ["s5-1", "s5-2"])

    def test_s6(self):
        formheader_codes = sorted(
            [
                header.code
                for header in TutorialTopic.query.filter_by(code="s6_cog")
                .first()
                .form_headers
            ]
        )
        self.assertEqual(formheader_codes, ["s6-1"])

    def test_s7(self):
        formheader_codes = sorted(
            [
                header.code
                for header in TutorialTopic.query.filter_by(code="s7_meta")
                .first()
                .form_headers
            ]
        )
        self.assertEqual(formheader_codes, ["s7-1"])


class QuestionsFromInstruments(BaseTestCase):
    """ Tests if randomisation works and if right number of questions collected.
    """

    def test_randomisation(self):
        """ Checks randomisation of questions by instrument.

        Creates two lists by calling the function on each instrument twice. If
        randomise is true, the two lists should not be equal. If randomise is
        false, the lists should be equal.

        Within the clause where instrument.randomise is true, the second list
        will be recreated while the two lists are equal.
        """
        for instrument in Instrument.query.all():
            with self.subTest(instrument=instrument):
                questions_1 = questions_from_instruments([instrument])
                questions_2 = questions_from_instruments([instrument])

                if instrument.randomise and len(questions_1) > 1:
                    while questions_1 == questions_2:
                        questions_2 = questions_from_instruments([instrument])
                    self.assertNotEqual(questions_1, questions_2)
                    self.assertEqual(len(questions_1), len(questions_2))
                else:
                    self.assertEqual(questions_1, questions_2)

    def test_number_of_questions(self):
        """Compares the number of questions collected with the function with the
        number of questions column of the Instrument table.
        """

        for instrument in Instrument.query.all():
            with self.subTest(instrument=instrument):
                self.assertEqual(
                    len(questions_from_instruments([instrument])),
                    instrument.number_of_questions,
                )


class SectionByFormHeaderAndInstrumentViewObject(BaseTestCase):
    """ Checks contents of instrument view objects and number of instrument view
    objects in each section by formheader.
    """

    def test_number_of_instrument_view_objects(self):
        """ Checks if the number of instrument view object is right for each
        formheader.

        Every formheader should have one instrument view object except for
        header s1-2 and s1-3.
        """

        for header in FormHeader.query.all():
            with self.subTest(header=header):

                if header.code == "s1-2":
                    self.assertEqual(
                        len(list(section_by_formheader(header))), 2
                    )
                elif header.code == "s1-3":
                    self.assertEqual(
                        len(list(section_by_formheader(header))), 3
                    )
                else:
                    self.assertEqual(
                        len(list(section_by_formheader(header))), 1
                    )

    def test_response_type_and_likert_attributes_of_non_rlq_instrument_view_objects(
        self
    ):
        """ Checks the attributes of the instrument view objects created that
        do not have the name "RLQ". Attributes checked are:
        - response_type
        - likert_min
        - likert_min_label
        - likert_max
        - likert_max_label
        - likert_intermediate_labels

        Since most of the instrument view objects are created by combining
        questions from different instruments with the same name, the attributes
        will be compared to its equivalent attributes with all the instruments
        with the same name.
        """

        unique_instrument_names = set(
            instrument.name
            for instrument in Instrument.query.all()
            if instrument.name != "RLQ"
        )

        for name in unique_instrument_names:
            instruments = Instrument.query.filter_by(name=name).all()
            instrument_view = instrument_view_object(instruments)
            for instrument in instruments:
                self.assertEqual(
                    instrument.response_type, instrument_view["response_type"]
                )
                self.assertEqual(
                    instrument.likert_min, instrument_view["likert_min"]
                )
                self.assertEqual(
                    instrument.likert_min_label,
                    instrument_view["likert_min_label"],
                )
                self.assertEqual(
                    instrument.likert_max, instrument_view["likert_max"]
                )
                self.assertEqual(
                    instrument.likert_max_label,
                    instrument_view["likert_max_label"],
                )
                self.assertEqual(
                    list_of_intermediate_labels(instrument),
                    instrument_view["likert_intermediate_labels"],
                )

    def test_code_attribute_of_non_rlq_instrument_view_objects(self):
        """ Checks the code attribute of the instrument view objects created that
        do not have the name "RLQ".

        Since most of the instrument view objects are created by combining
        questions from different instruments with the same name, the attributes
        will be compared to its equivalent attributes with all the instruments
        with the same name.
        """

        unique_instrument_names = set(
            instrument.name
            for instrument in Instrument.query.all()
            if instrument.name != "RLQ"
        )

        for name in unique_instrument_names:
            instruments = Instrument.query.filter_by(name=name).all()
            instrument_codes = [instrument.code for instrument in instruments]

            instrument_view = instrument_view_object(instruments)
            self.assertIn(instrument_view["code"], instrument_codes)

    def test_questions_attribute_of_non_rlq_instrument_view_objects(self):
        """ Checks the questions attribute of the instrument view objects created that
        do not have the name "RLQ". Check will be that the number of questions
        should equal the sum of the number of questions for the instruments.

        Since most of the instrument view objects are created by combining
        questions from different instruments with the same name, the attributes
        will be compared to its equivalent attributes with all the instruments
        with the same name.
        """

        unique_instrument_names = set(
            instrument.name
            for instrument in Instrument.query.all()
            if instrument.name != "RLQ"
        )

        for name in unique_instrument_names:
            instruments = Instrument.query.filter_by(name=name).all()

            instrument_view = instrument_view_object(instruments)
            self.assertEqual(
                len(instrument_view["questions"]),
                sum(
                    instrument.number_of_questions for instrument in instruments
                ),
            )

    def test_response_type_and_likert_attributes_of_rlq_instrument_view_objects(
        self
    ):
        """ Checks the attributes of the instrument view objects created that
        have the name "RLQ". Attributes checked are:
        - response_type
        - likert_min
        - likert_min_label
        - likert_max
        - likert_max_label
        - likert_intermediate_labels

        Since most of the instrument view objects are created by combining
        questions from different instruments with the same name, the attributes
        will be compared to its equivalent attributes with all the instruments
        with the same name.

        For convenience, we can use the formheader's relationship to instruments
        to get all the relevant s2 instruments for each formheader.
        """

        for formheader in FormHeader.query.filter_by(
            tutorial_topic_code="s2_rlq"
        ).all():
            instruments = formheader.instruments
            instrument_view = instrument_view_object(instruments)
            for instrument in instruments:
                self.assertEqual(
                    instrument.response_type, instrument_view["response_type"]
                )
                self.assertEqual(
                    instrument.likert_min, instrument_view["likert_min"]
                )
                self.assertEqual(
                    instrument.likert_min_label,
                    instrument_view["likert_min_label"],
                )
                self.assertEqual(
                    instrument.likert_max, instrument_view["likert_max"]
                )
                self.assertEqual(
                    instrument.likert_max_label,
                    instrument_view["likert_max_label"],
                )
                self.assertEqual(
                    list_of_intermediate_labels(instrument),
                    instrument_view["likert_intermediate_labels"],
                )

    def test_code_attribute_of_rlq_instrument_view_objects(self):
        """ Checks the code attribute of the instrument view objects created that
        have the name "RLQ".

        Since most of the instrument view objects are created by combining
        questions from different instruments with the same name, the attributes
        will be compared to its equivalent attributes with all the instruments
        with the same name.

        For convenience, we can use the formheader's relationship to instruments
        to get all the relevant s2 instruments for each formheader.
        """

        for formheader in FormHeader.query.filter_by(
            tutorial_topic_code="s2_rlq"
        ):
            instruments = formheader.instruments
            instrument_codes = [instrument.code for instrument in instruments]

            instrument_view = instrument_view_object(instruments)
            self.assertIn(instrument_view["code"], instrument_codes)

    def test_questions_attribute_of_rlq_instrument_view_objects(self):
        """ Checks the questions attribute of the instrument view objects created that
        have the name "RLQ". Check will be that the number of questions
        should equal the sum of the number of questions for the instruments.

        Since most of the instrument view objects are created by combining
        questions from different instruments with the same name, the attributes
        will be compared to its equivalent attributes with all the instruments
        with the same name.

        For convenience, we can use the formheader's relationship to instruments
        to get all the relevant s2 instruments for each formheader.
        """

        for formheader in FormHeader.query.filter_by(
            tutorial_topic_code="s2_rlq"
        ):
            instruments = formheader.instruments

            instrument_view = instrument_view_object(instruments)
            self.assertEqual(
                len(instrument_view["questions"]),
                sum(
                    instrument.number_of_questions for instrument in instruments
                ),
            )


class FormHeadersAndInstruments(BaseTestCase):
    """Checks the number of formheader objects for each tutorial topic."""

    def test_number_of_formheader_objects(self):
        for topic in TutorialTopic.query.all():
            self.assertEqual(
                len(formheaders_and_instruments(topic.form_headers)),
                len(topic.form_headers),
            )


class RenderedQuestionnaireContentTests(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=test_user)
        cls.app.post("/auth/login", data=test_user)

    def test_instructions_populated(self):
        """ Test to see if instructions are populated.

        There is a fringe case for how apostrophes are rendered in HTML. It is
        replaced with its ASCII code. So a conditional is used to check whether
        apostrophes are in the instruction, and it is replcaed with the ASCII
        code if it is found.
        """
        for topic in TutorialTopic.query.all():
            response = self.app.get(
                f"/questionnaire/{topic.code}", follow_redirects=False
            )
            for header in topic.form_headers:
                with self.subTest(topic=topic.code, header=header.code):
                    instruction = header.instruction.replace("'", "&#39;")
                    self.assertIn(bytes(instruction, "utf-8"), response.data)

    def test_questions_populated(self):
        """ Tests to see if questions were populated.

        Checks for the questions and the question codes.
        """
        for topic in TutorialTopic.query.all():
            response = self.app.get(
                f"/questionnaire/{topic.code}", follow_redirects=False
            )
        for instrument in topic.instruments:
            for question in instrument.questions:
                with self.subTest(
                    topic=topic.code,
                    instrument=instrument.code,
                    question=question.code,
                ):
                    self.assertIn(
                        bytes(question.statement, "utf-8"), response.data
                    )
                    self.assertIn(bytes(question.code, "utf-8"), response.data)

    @staticmethod
    def expected_number_of_input_in_tutorial_topic_with_response_type(
        topic, response_type
    ):
        """ Helper method to get expected number of input.

        For instruments with likert response type, the expected number of ihput
        will be the number of questions within that instrument multiplied by the
        number of the max likert.

        For checkbox, it will simply be the number of questions.

        For truefalse, it will be double the number of questions.
        """
        if response_type == "likert":
            return sum(
                1 * int(instrument.likert_max)
                for instrument in topic.instruments
                for question in instrument.questions
                if instrument.response_type == response_type
            )
        elif response_type == "checkbox":
            return sum(
                1
                for instrument in topic.instruments
                for question in instrument.questions
                if instrument.response_type == response_type
            )
        elif response_type == "truefalse":
            return sum(
                1 * 2
                for instrument in topic.instruments
                for question in instrument.questions
                if instrument.response_type == response_type
            )

    def test_number_of_radio_buttons_populated(self):
        """Test for the number of radio input tags"""
        for topic in TutorialTopic.query.all():
            response = self.app.get(
                f"/questionnaire/{topic.code}", follow_redirects=False
            )
            with self.subTest(topic=topic.code):
                if topic.code != "s7_meta":
                    self.assertEqual(
                        response.data.count(b'<input type="radio"'),
                        self.expected_number_of_input_in_tutorial_topic_with_response_type(
                            topic, "likert"
                        ),
                    )
                else:
                    self.assertEqual(
                        response.data.count(b'<input type="radio"'),
                        self.expected_number_of_input_in_tutorial_topic_with_response_type(
                            topic, "truefalse"
                        ),
                    )

    def test_number_of_checkbox_buttons_populated_for_checkbox_instruments(
        self
    ):
        """Test for the number of checkboxes for instruments with checkbox
        response type.

        All instruments checked except for s7_meta.
        """
        for topic in TutorialTopic.query.all():
            response = self.app.get(
                f"/questionnaire/{topic.code}", follow_redirects=False
            )
            with self.subTest(topic=topic.code):
                self.assertEqual(
                    response.data.count(b'<input type="checkbox"'),
                    self.expected_number_of_input_in_tutorial_topic_with_response_type(
                        TutorialTopic.query.filter_by(code=topic.code).first(),
                        "checkbox",
                    ),
                )


if __name__ == "__main__":
    unittest.main()
