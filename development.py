from questionnaire import create_app

app = create_app(production=False)

if __name__ == "__main__":
    app.run()
