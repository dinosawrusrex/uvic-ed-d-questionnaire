# Survey Tool for UVic

A custom survey tool built using [Flask](https://flask.palletsprojects.com/en/1.1.x/). Registered users respond to surveys containing one or more instruments and receive summaries after. Each instrument contains a set of statements and has an individualised method of computing its summary. A sample of the application can be found [here](https://samplecustomsurveytool.dinosawrusrex.ca).

## 1 Usage

Click [here](https://samplecustomsurveytool.dinosawrusrex.ca) for a live sample.

## 2 Contributing

Plenty of things to improve and build on! Reach out for bugs or feature requests via the issues tab or email.

### 2.1 Contribute With Code

Please reach out if you are wanting to get a local server running. There are some extra files and perhaps configuration tips to pass on. The extra files are to populate the database with current instruments and statements. I have set up the app factory to obtain configuration from a directory called `instance` which is ignored for version control purposes.

Code I have written is formatted with [Black](https://black.readthedocs.io/en/stable/), and I hope to continue this with code coming in.

#### 2.1.1 Basics

Install the latest Python 3.7.4. Refer to Python's [download page](https://www.python.org/downloads/) to download the appropriate installer for your operating system. Real Python has a good [walkthrough](https://realpython.com/installing-python/) of the installation.

Check the version of Python installed on your system on the command line prompt (terminal or CMD for Windows):

- `python --version`
- `python3 --version` (in the event you have Python 2 installed)
- `py --version` (in Windows, if user did not explicitly add Python to PATH)

Download this repository and unzip the directory. Then navigate to the "root" directory, which contains `README.md` and the `questionnaire` directory.

Create a virtual environment: `python -m venv edd-questionnaire`

Activate the virtual environment: `source edd-questionnaire/bin/activate` or `edd-questionnaire\Scripts\activate.bat` for Windows

Install the requirements: `pip install -r requirements.txt`

#### 2.1.2 Database Setup

I have the database connection set up in the config file. However, it is possible to just set up the connection in the [application factory](questionnaire/__init__.py). I would refer to [Flasks' own tutorial](https://flask.palletsprojects.com/en/1.1.x/tutorial/factory/) to do this with SQLite since SQLite comes with Python's Standard Library.

Once you have the question banks, and have the database connection ready, run `flask db upgrade` which sets up the database with the provided schema. Then, run `flask init-db` to populate the database. If everything ran without errors, run `python development.py` and navigate to `localhost:5000`.

## 3 Thanks

This project has been a wonderful learning experience as a software developer. I am grateful to be able to work with some hardworking researchers and instructors. I am also thankful for my friend who reached out to get me involved with this project.
