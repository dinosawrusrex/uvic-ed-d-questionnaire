from flask import current_app
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, SubmitField, PasswordField, validators


class RegistrationForm(FlaskForm):

    username = StringField("Username", [validators.InputRequired("Username required")])
    first_name = StringField(
        "First name", [validators.InputRequired("First name required")]
    )
    last_name = StringField("Last name", [validators.InputRequired("Last name required")])
    student_id_last_three_digits = StringField(
        "Last three digits of student ID",
        [
            validators.InputRequired("Last three digits of student ID required"),
            validators.Regexp(r"^\d{3}$", message="Digits only"),
        ],
    )
    password = PasswordField("Password", [validators.InputRequired("Password required")])
    password_confirm = PasswordField(
        "Confirm password",
        [
            validators.InputRequired("Password confirmation required"),
            validators.EqualTo("password", message="Passwords must match"),
        ],
    )
    submit = SubmitField("Register")


class LoginForm(FlaskForm):

    username = StringField("Username", [validators.InputRequired("Username required")])
    password = PasswordField("Password", [validators.InputRequired("Password required")])
    submit = SubmitField("Log In")


class PasswordResetForm(FlaskForm):

    username = StringField("Username", [validators.InputRequired("Username required")])
    student_id_last_three_digits = StringField(
        "Last three digits of your student ID",
        [
            validators.InputRequired("Last three digits of student ID required"),
            validators.Regexp(r"^\d{3}$", message="Digits only"),
        ],
    )
    password = PasswordField(
        "New password", [validators.InputRequired("Please enter new password")]
    )
    password_confirm = PasswordField(
        "Confirm new password",
        [
            validators.InputRequired("Password confirmation required"),
            validators.EqualTo("password", message="Passwords must match"),
        ],
    )
    submit = SubmitField("Reset Password")


class PasswordChangeForm(FlaskForm):

    password = PasswordField(
        "New password", [validators.InputRequired("Please enter new password")]
    )
    password_confirm = PasswordField(
        "Confirm new password",
        [
            validators.InputRequired("Password confirmation required"),
            validators.EqualTo("password", message="Passwords must match"),
        ],
    )
    submit = SubmitField("Change Password")


class GetUsernameForm(FlaskForm):

    first_name = StringField(
        "First name", [validators.InputRequired("First name required")]
    )
    last_name = StringField("Last name", [validators.InputRequired("Last name required")])
    student_id_last_three_digits = StringField(
        "Last three digits of your student ID",
        [
            validators.InputRequired("Last three digits of student ID required"),
            validators.Regexp(r"^\d{3}$", message="Digits only"),
        ],
    )
    submit = SubmitField("Get Username")
