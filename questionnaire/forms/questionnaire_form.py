from wtforms import Form, RadioField


def likert(statements, start, end):
    class _Likert(Form):
        pass

    for statement in statements:
        setattr(
            _Likert,
            str(statement["id"]),
            RadioField(
                statement["question"],
                choices=[(value, value) for value in range(start, end + 1)],
            ),
        )

    return _Likert()
