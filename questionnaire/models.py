from datetime import datetime
from questionnaire import db
from werkzeug.security import check_password_hash, generate_password_hash


class Question(db.Model):
    __tablename__ = "question"
    code = db.Column(db.String(10), primary_key=True)
    statement = db.Column(db.Text)
    reversed_score = db.Column(db.Boolean)
    instrument_code = db.Column(db.String(10), db.ForeignKey("instrument.code"))
    responses = db.relationship("Response", backref="question", lazy=True)

    def __repr__(self):
        return f"Question {self.code}"


class Instrument(db.Model):
    __tablename__ = "instrument"
    code = db.Column(db.String(10), primary_key=True)
    name = db.Column(db.Text)
    subfactor = db.Column(db.Text)
    randomise = db.Column(db.Boolean)
    response_type = db.Column(db.String(20))
    likert_min = db.Column(db.Integer)
    likert_min_label = db.Column(db.Text)
    likert_max = db.Column(db.Integer)
    likert_max_label = db.Column(db.Text)
    likert_intermediate_labels = db.Column(db.Text)
    scoring_method = db.Column(db.Text)
    form_header_code = db.Column(
        db.String(10), db.ForeignKey("form_header.code")
    )
    number_of_questions = db.Column(db.Integer)
    report_description = db.Column(db.Text)
    tutorial_topic_code = db.Column(
        db.String(64), db.ForeignKey("tutorial_topic.code")
    )
    questions = db.relationship("Question", backref="instrument", lazy=True)

    def __repr__(self):
        return f"Instrument {self.code}"


class FormHeader(db.Model):
    __tablename__ = "form_header"
    code = db.Column(db.String(10), primary_key=True)
    header = db.Column(db.Text)
    instruction = db.Column(db.Text)
    tutorial_topic_code = db.Column(
        db.String(64), db.ForeignKey("tutorial_topic.code")
    )
    instruments = db.relationship("Instrument", backref="formheader", lazy=True)

    def __repr__(self):
        return f"FormHeader {self.code}"


class TutorialTopic(db.Model):
    __tablename__ = "tutorial_topic"
    code = db.Column(db.String(64), primary_key=True)
    topic = db.Column(db.Text)
    instruments = db.relationship(
        "Instrument", backref="tutorialtopic", lazy=True
    )
    form_headers = db.relationship(
        "FormHeader", backref="tutorialtopic", lazy=True
    )
    sessions = db.relationship("Session", backref="tutorialtopic", lazy=True)

    def __repr__(self):
        return f"TutorialTopic {self.code}"


class Student(db.Model):
    __tablename__ = "student"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    student_id_last_three_digits = db.Column(db.Integer)
    username = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.String(128))
    admin = db.Column(db.Boolean)
    sessions = db.relationship("Session", backref="student", lazy=True)

    def __repr__(self):
        return f"Student {self.username}"

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def hash_password(password):
        return generate_password_hash(password)


class Session(db.Model):
    __tablename__ = "session"
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    student_id = db.Column(db.Integer, db.ForeignKey("student.id"))
    tutorial_topic_code = db.Column(
        db.String(64), db.ForeignKey("tutorial_topic.code")
    )
    responses = db.relationship("Response", backref="session", lazy=True)


class Response(db.Model):
    __tablename__ = "response"
    id = db.Column(db.Integer, primary_key=True)
    question_code = db.Column(db.String(10), db.ForeignKey("question.code"))
    value = db.Column(db.String(6))
    session_id = db.Column(db.Integer, db.ForeignKey("session.id"))

    def __repr__(self):
        return f"Response for {self.question_code}: {self.value}"
