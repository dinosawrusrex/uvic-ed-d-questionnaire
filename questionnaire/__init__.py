from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_talisman import Talisman
import os


db = SQLAlchemy()
migrate = Migrate()
talisman = Talisman()


def create_app(production=False):
    app = Flask(__name__, instance_relative_config=True)

    from instance import config

    if production:
        app.config.from_object(config.ProductionConfig(app))
    else:
        app.config.from_object(config.DevelopmentConfig(app))

    db.init_app(app)
    migrate.init_app(app, db)
    talisman.init_app(
        app,
        content_security_policy={
            "default-src": [
                "'self'",
                "fonts.googleapis.com",
                "fonts.gstatic.com",
                "code.jquery.com",
                "cdn.jsdelivr.net",
                "stackpath.bootstrapcdn.com",
                "kit.fontawesome.com",
                "kit-free.fontawesome.com",
            ]
        },
    )

    from questionnaire import models, database

    app.cli.add_command(database.init_database)

    from .blueprints import authentication, index, questionnaire, results_display

    app.register_blueprint(index.bp)
    app.register_blueprint(authentication.bp)
    app.register_blueprint(questionnaire.bp)
    app.register_blueprint(results_display.bp)

    from .blueprints import error_handler

    app.register_error_handler(404, error_handler.handle_404)
    app.register_error_handler(500, error_handler.handle_500)

    @app.shell_context_processor
    def make_shell_context():
        return {
            "db": db,
            "Question": models.Question,
            "Instrument": models.Instrument,
            "FormHeader": models.FormHeader,
            "TutorialTopic": models.TutorialTopic,
            "Student": models.Student,
            "Session": models.Session,
            "Response": models.Response,
        }

    return app
