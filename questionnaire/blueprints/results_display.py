from flask import Blueprint, g, render_template, request, session
from sqlalchemy import or_
from datetime import datetime
from questionnaire import db
from questionnaire.blueprints.authentication import login_required
from questionnaire.models import Instrument, Response, Session, Student, TutorialTopic
import statistics

bp = Blueprint("results_display", __name__)


@bp.route("/output", methods=("GET", "POST"))
def output():
    if request.method == "POST":
        tutorial_topic_code = session.pop("tutorial_topic_code")
        add_session(tutorial_topic_code)

        current_session = Session.query.order_by(Session.timestamp.desc()).first().id
        add_responses(tutorial_topic_code, current_session)
        db.session.commit()

    output = produce_output_view(tutorial_topic_code, current_session)
    return render_template(
        output["template"], title=output["title"], summary=output["summary"]
    )


@bp.route("/responses", methods=("GET", "POST"))
@login_required
def responses():
    sessions = Student.query.filter_by(id=g.student.id).first().sessions
    tutorial = lambda code: TutorialTopic.query.filter_by(code=code).first().topic
    return render_template("responses.html", sessions=sessions, tutorial=tutorial)


@bp.route("/responses/<int:session_id>")
@login_required
def response_from_session(session_id):

    # Gets session or returns 404 if session not found
    selected_session = Session.query.filter_by(id=session_id).first_or_404()

    # Make sure session belongs to student
    if selected_session.student_id != g.student.id:
        return render_template("404.html")

    tutorial_topic = selected_session.tutorial_topic_code

    output = produce_output_view(tutorial_topic, session_id)
    return render_template(
        output["template"], title=output["title"], summary=output["summary"]
    )


def produce_output_view(tutorial_topic_code, session_id):
    title = tutorial_topic_code.replace("_", " ").title()
    title = TutorialTopic.query.filter_by(code=tutorial_topic_code).first().topic

    if tutorial_topic_code in ["s1_learn", "s4_beh"]:
        return {
            "template": "summary_views/output.html",
            "title": title,
            "summary": score_and_instruments(tutorial_topic_code, session_id),
        }

    elif tutorial_topic_code == "s5_socemo":
        return {
            "template": "summary_views/output.html",
            "title": title,
            "summary": compute_s5_summary(session_id),
        }

    elif tutorial_topic_code == "s6_cog":
        return {
            "template": "summary_views/s6_output.html",
            "title": title,
            "summary": score_and_instruments(tutorial_topic_code, session_id),
        }

    elif tutorial_topic_code == "s7_meta":
        return {
            "template": "summary_views/s7_output.html",
            "title": title,
            "summary": instrument_code_score_and_instrument(
                tutorial_topic_code, session_id
            ),
        }

    else:
        return {
            "template": "summary_views/s2_output.html",
            "title": title,
            "summary": instrument_code_score_and_instrument(
                tutorial_topic_code, session_id
            ),
        }


def add_session(tutorial_topic_code):
    db.session.add(
        Session(
            **{"student_id": g.student.id, "tutorial_topic_code": tutorial_topic_code}
        )
    )


def add_responses(tutorial_topic_code, current_session):
    for instrument in (
        TutorialTopic.query.filter_by(code=tutorial_topic_code).first().instruments
    ):
        for question in instrument.questions:
            db.session.add(
                Response(
                    **{
                        "question_code": question.code,
                        "value": request.form.get(question.code),
                        "session_id": current_session,
                    }
                )
            )


def score_and_instruments(tutorial_topic_code, session_id):
    instruments = Instrument.query.filter_by(
        tutorial_topic_code=tutorial_topic_code
    ).all()
    return (
        {"instrument": instrument, "score": compute_score(instrument, session_id)}
        for instrument in instruments
    )


def instrument_code_score_and_instrument(tutorial_topic_code, session_id):
    instruments = Instrument.query.filter_by(
        tutorial_topic_code=tutorial_topic_code
    ).all()
    return {
        instrument.code: {
            "score": compute_score(instrument, session_id),
            "instrument": instrument,
        }
        for instrument in instruments
    }


def compute_s5_summary(session_id):
    instrument_sb = Instrument.query.filter_by(code="sb").first()
    return [
        {"instrument": instrument_sb, "score": compute_score(instrument_sb, session_id)},
        {
            "instrument": Instrument.query.filter_by(code="mh-e").first(),
            "score": compute_mental_health_continuum_output(session_id),
        },
    ]


def compute_score(instrument, session_id):
    if instrument.response_type == "likert":
        return calculate_likert_mean(instrument, session_id)
    elif instrument.response_type == "checkbox":
        return calculate_checkbox_mean(instrument, session_id)
    else:
        return calculate_truefalse_mean(instrument, session_id)


def calculate_likert_mean(instrument, session_id):
    """
    Returns the mean for the responses of likert instrument that are given.
    Statements without responses are not included in the calculation, as advised
    by instructor.
    """
    responses = [
        int(response.value)
        for question in instrument.questions
        for response in Response.query.filter_by(
            session_id=session_id, question_code=question.code
        ).all()
        if response.value
    ]
    if responses:
        return round(statistics.mean(responses), 2)
    return "N/A"


def calculate_checkbox_mean(instrument, session_id):
    """
    Returns the mean for the responses of checkbox instrument that are given.
    Statements without responses are not included in the calculation, as advised
    by instructor.
    """
    responses = [
        5 if response.value else 0
        for question in instrument.questions
        for response in Response.query.filter_by(
            session_id=session_id, question_code=question.code
        ).all()
    ]
    if responses:
        return round(statistics.mean(responses), 2)
    return "N/A"


def calculate_truefalse_mean(instrument, session_id):
    """
    Returns the mean for the responses of truefalse instrument that are given.
    Statements without responses are not included in the calculation, as advised
    by instructor.
    """
    responses = []
    for question in instrument.questions:
        for response in Response.query.filter_by(
            session_id=session_id, question_code=question.code
        ).all():
            if response.value == "True":
                responses.append(5)
            elif response.value == "False":
                responses.append(0)
    if responses:
        return round(statistics.mean(responses), 2)
    return "N/A"


def compute_mental_health_continuum_output(session_id):
    """
    Returns the output of the mental health continuum instrument.

    Emotional well-being will have the acronym ewb.
    Social well-being will have the acronym swb.
    Psychological well-being will have the acronym pwb.
    """
    ewb_scores = []
    swb_and_pwb_scores = []

    ewb_question_codes = list(
        map(
            lambda question: question.code,
            Instrument.query.filter_by(code="mh-e").first().questions,
        )
    )
    swb_and_pwb_question_codes = [
        question.code
        for instrument in Instrument.query.filter(
            or_(Instrument.code == "mh-s", Instrument.code == "mh-p")
        ).all()
        for question in instrument.questions
    ]

    for responses in Session.query.filter_by(id=session_id).first().responses:
        if responses.question_code in ewb_question_codes:
            ewb_scores.append(int(responses.value) if responses.value else None)
        elif responses.question_code in swb_and_pwb_question_codes:
            swb_and_pwb_scores.append(int(responses.value) if responses.value else None)

    if (ewb_scores.count(4) > 0 or ewb_scores.count(5) > 0) and (
        swb_and_pwb_scores.count(4) > 5 or swb_and_pwb_scores.count(5) > 5
    ):
        return "Fluorishing"
    elif (ewb_scores.count(1) > 0 or ewb_scores.count(2) > 0) and (
        swb_and_pwb_scores.count(1) > 5 or swb_and_pwb_scores.count(2) > 5
    ):
        return "Languishing"

    return "Moderate"


@bp.route("/export-all-responses", methods=("GET", "POST"))
@login_required
def export_all_responses():
    if not g.student.admin:
        return render_template("404.html")

    topics = TutorialTopic.query.all()
    question_codes = []
    responses = []
    topic = request.form.get("topic_code")

    if request.method == "POST" and topic:
        students_to_exclude = [
            student.id
            for student in Student.query.all()
            if student.username in ["asdasd", "edd101"]
        ]
        sessions = [
            session
            for session in Session.query.filter_by(tutorial_topic_code=topic).all()
            if session.student_id not in students_to_exclude
        ]
        question_codes = [
            question.code
            for instrument in TutorialTopic.query.filter_by(code=topic)
            .first()
            .instruments
            for question in instrument.questions
        ]

        for session in sessions:
            student = Student.query.filter_by(id=session.student_id).first()
            responses.append(
                (
                    student.first_name,
                    student.last_name,
                    student.student_id_last_three_digits,
                    session.timestamp,
                    {
                        response.question_code: response.value if response.value else "."
                        for response in session.responses
                    },
                )
            )
    return render_template(
        "summary_views/all_responses.html",
        topics=topics,
        question_codes=question_codes,
        responses=responses,
    )
